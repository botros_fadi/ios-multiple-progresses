//
//  AppDelegate.h
//  SomeReactivityDemo
//
//  Created by fadi on 8/7/18.
//  Copyright © 2018 fadi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

