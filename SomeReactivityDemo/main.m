//
//  main.m
//  SomeReactivityDemo
//
//  Created by fadi on 8/7/18.
//  Copyright © 2018 fadi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
